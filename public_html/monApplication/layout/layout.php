<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  
  
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
       <link rel="shortcut icon" href="image/care.png" type="image/icon"/>
      <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
   
    <title>CERIGO</title>
  </head><body class="text-center">
	  
	  <!--navbar--->
  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed top">
  <a class="navbar-brand" href="monApplication.php" >CERIGO</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="monApplication.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="inscription">inscrirption</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="connexion">Connexion</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="compte" style="display: none;">Compte</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" id="deconnexion" style="display: none;">Deconnexion</a>
      </li>
     
     
    </ul>
  </div>
</nav>
	



 <center><h2><style color="white"></h2></center></style>
    <?php if($context->getSessionAttribute('user_id')): ?>
  <?php echo $context->getSessionAttribute('user_id')." est connecte"; ?>
     <?php endif; ?>

    <div id="page">
      <?php if($context->error): ?>
        <div id="flash_error" class="error">
          <?php echo " $context->error !!!!!" ?>
        </div>
      <?php endif; ?>



<div id="page_maincontent"> 
	 	    
<?php 
  include($template_view); 
  ?>
</div>
</div>
      
<div class= "text">
  
</div>

  <script src="js/voyage.js" type="text/javascript"></script>
  </body>

</html>
