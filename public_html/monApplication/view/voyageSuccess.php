<link rel="stylesheet" type="text/css" href="css/bootstrap/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" href="css/perfect-scrollbar.css">
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<body>
 
<?php if ($context->voyage != NULL)
 {?>
	 <p style="color: darkblue"> Les trajets disponibles !</p>
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
	       <div class="table100 ver1 m-b-110">
	         <div class="table100-head">
			<table>
				<thead>
		           <tr class="row100 head">
	                   <th class="cell100 column1">Conducteur</th>
	                   <th class="cell100 column2">Départ</th>
	                   <th class="cell100 column3">Arrivée</th>
	                   <th class="cell100 column4">Tarif</th>
	                   <th class="cell100 column5">NbPlace</th>
	                   <th class="cell100 column6">Heuredepart</th>
	                    <th class="cell100 column7">Contraintes</th>
	                    
					   </tr>
					 </thead>
					</table>
				</div>
			<div class="table100-body js-pscroll">
				<table>
					 <tbody>
	                 <?php foreach ($context->voyage as $value)
	                  {?>
	                   <tr  class="row100 head">
		            	 	<td class="cell100 column1"><?php echo $value->conducteur->nom. "  " . $value->conducteur->prenom; ?></td>
		            	 	<td class="cell100 column2"><?php echo $value->trajet->depart; ?> </td>
		            	 	<td class="cell100 column3"><?php echo $value->trajet->arrivee; ?></td>
		            	 	<td class="cell100 column4"><?php echo $value->tarif. '€'?></td>
		            	 	<td class="cell100 column5"><?php echo $value->nbplace; ?></td>
		               		<td class="cell100 column6"><?php echo $value->heuredepart. 'h' ?></td>
                        	<td class="cell100 column7"><?php echo $value->contraintes; ?></td>
                        	
		            	 </tr>
						   <?php    }  ?>
						   
					  </tbody>
					  </table>
					</div>
				</div>
				</div>
		</div>
	</div>
<?php }?>
<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>	            	
	
</body>
